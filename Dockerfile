FROM        ubuntu:16.04

LABEL       author="Shirobaka" maintainer="info@six-gaming.com"

RUN         apt update \
            && apt upgrade -y \
            && apt install -y libstdc++6 lib32stdc++6 tar curl iproute2 openssl tzdata \
			&& useradd -d /home/container -m container \
			&& ln -snf /usr/share/zoneinfo/Europe/Berlin /etc/localtime && echo Europe/Berlin > /etc/timezone \
			&& dpkg-reconfigure --frontend noninteractive tzdata \
			&& ls -l /etc/localtime

USER        container
ENV         USER=container HOME=/home/container

WORKDIR     /home/container

COPY        ./entrypoint.sh /entrypoint.sh

CMD ["/bin/bash", "/entrypoint.sh"]